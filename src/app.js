import Chart from './View/Chart'
import data from './mock'


new Chart({
    width: 700,
    height: 700,
    data,
    container: document.getElementById('main')
})

