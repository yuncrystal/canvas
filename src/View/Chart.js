/**
 * Task
 * 
 * 1. Drawing the coordinate axis
 *     The scale of x-axis is 5 minutes
 *     The scale of y-axis is determined by the mock data (highest price - lowest -price)/10
 * 
 * 2. Draw a Candlestick indicator
 * 
 * 3. The chart has panning behavior
 * 
 * 4. Draw a Rectangle shape
 * 
 * 5. Bonus: Can draw shapes on mobile
 * 
 * 6. Bonus: Add zooming chart behavior
 *
 * You can only use Concrete.js and d3 libraries,
 * Do not use any drawing library like Chart.js
 */


export default class Clart {
    constructor({
        container,
        data,
        width,
        height,
    }) {
        this.container = container
        this.data = data
    }


    // Triggers a redraw of all chart elements.
    render() {

    }

    // Destroy all shapes and indicators
    destory() {

    }

    // Remove shape by shape id
    removeShape(shapeId) {

    }

    /**
     * Add a shape on canvas by type
     */
    addShapeByType(type) {

    }

    /** Add a indicator on canvas by type
     * 
    */
    addIndicatorByType(type) {

    }

    


}

