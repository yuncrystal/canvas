const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
module.exports = () => {
  return {
    entry: path.resolve(__dirname, "src", "app.js"),
    mode: "development",
    devtool: "inline-source-map",
    module: {
      rules: [
        {
          test: /\.js?$|.ts?$/,
          include: [path.resolve(__dirname, "src")],
          use: ["babel-loader"]
        }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, "src/index.html")
      })
    ]
  };
};
